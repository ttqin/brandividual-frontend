'use strict';

/* Services */

var brandividualServices = angular.module('brandividualServices', ['ngResource']);

brandividualServices.factory('Product', ['$resource',
  function($resource){
    return $resource('products/:productId.json', {}, {
      query: {method:'GET', params:{productId:'products'}, isArray:true}
    });
  }]);
