'use strict';

/* App Module */

var brandividualApp = angular.module('brandividualApp', [
  'ngRoute',
  'brandividualControllers',
  'phonecatFilters',
  'brandividualServices'
]);

brandividualApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/product', {
        templateUrl: 'partials/product-list.html',
        controller: 'ProductListCtrl'
      }).
      when('/product/:productId', {
        templateUrl: 'partials/product-detail.html',
        controller: 'ProductDetailCtrl'
      }).
      otherwise({
        redirectTo: '/product'
      });
  }]);
